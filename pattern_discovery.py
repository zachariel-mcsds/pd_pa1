#!/usr/bin/env python

import os
import pandas as pd
import logging
import itertools

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def testing_dataset():
    itemset = [
        [{1, 2, 3, 4}],
        [{1, 2, 4}],
        [{1, 2, 5}],
        [{2, 3, 4}],
        [{2, 3}],
        [{3, 4}],
        [{2, 4}],
        [{1, 3, 4, 5}],
        [{1, 2}],
        [{2, 3}],
        [{1, 3, 4}],
    ]

    dataset = pd.DataFrame(itemset, columns=['categories'])
    return dataset

def root_dir():
    """Project base directory"""
    return os.path.dirname(__file__)

def data_dir():
    """Data directory path"""
    return os.path.join(root_dir(), 'data')

def file_path():
    """Path to categories file"""
    return os.path.join(data_dir(), 'categories.txt')

def data(filename):
    """Dataframe imported from file path"""

    df = pd.DataFrame(columns=['categories'])
    for line in file_content(filename):
        df.loc[len(df)] = [line.strip().split(';')]

    return df

def file_content(filename):
    """Read file and yiel line by line"""

    with open(filename, 'r') as f:
        for line in f:
            yield line

def parse_categories(str_value):
    """Parse string representation of set object"""

    str_value = str_value.replace('{','').replace('}', '')
    str_value = str_value.split(', ')
    str_value = set([s.replace('\'', '').strip() for s in str_value])
    return str_value

class PatternDiscovery:
    """ Class to implement Apriori algorithm over dataset"""

    PATTERN='pattern'
    SUPPORT='support'
    K_TITLE = 'k'
    CATEGORIES = 'categories'

    def __init__(self, filename, min_support=771, csv=True):
        """Initizize object

        Params:
            filename: path to data file
            min_support: threshold to prune patterns
        """

        self.min_support = min_support
        self.k = 1
        self.categories = pd.DataFrame(columns=['categories'])
        self.frequents = pd.DataFrame(columns=[self.K_TITLE, self.PATTERN, self.SUPPORT])

        if type(filename) is pd.DataFrame:
            self.categories = filename
        else:
            self.filename = filename

            if not csv:
                self.import_file()
            #(filename)
            #self.categories['new_categories'] = self.categories['categories'].apply(parse_categories)
            #self.categories.drop(['Unnamed: 0', 'categories'], axis=1, inplace=True)
            #self.categories.columns = ['categories']

    def count(self, products):
        for product in products:
            self.counters.setdefault(product, 0)
            self.counters[product] += 1

    def read_file(self):
        logger.info("Reading file: %s",  self.filename)
        for line in file_content(self.filename):
            categories = frozenset(line.strip().split(';'))
            yield categories

    def import_file(self):
        for categories in self.read_file():
            self.categories.loc[len(self.categories)] = [categories]

    def find_frequent_1_itemset(self):
        logger.info("Generating single patterns")
        if self.categories.empty:
            logger.debug("Reading from CSV file")
            for categories in self.read_file():
                self.categories.loc[len(self.categories)] = [categories]
                self.increase_items_support(categories)
        else:
            logger.debug('Reading from DataFrame')
            for idx, row in self.categories.iterrows():
                items = row['categories']
                self.increase_items_support(items)
        self.prune(self.frequents)
        self.k = 2
        logger.debug("Total categories: %s", self.categories.shape[0])
        logger.debug("Total k=1 patterns: %s", self.frequents.shape[0])
        return self.frequents[self.frequents['k'] == 1][self.PATTERN]

    def increase_support(self, pattern, dF=None):
        #logger.debug("Increasing support for %s", pattern);
        dataFrame = None
        if type(dF) is pd.DataFrame:
            dataFrame = dF
        else:
            dataFrame = self.frequents

        matches = dataFrame[(dataFrame[self.K_TITLE] == self.k) & (dataFrame[self.PATTERN] == pattern)]
        if matches.empty:
            #logger.debug("Adding pattern to frequents %s", pattern)
            dataFrame.loc[len(dataFrame)] = [self.k, pattern, 1]
        else:
            for idx in matches.index:
                #current_support =  dataFrame.loc[idx, self.SUPPORT]
                #dataFrame.loc[idx, self.SUPPORT] = current_support + 1
                #current_support =  dataFrame.at[idx, self.SUPPORT]
                dataFrame.at[idx, self.SUPPORT] += 1 #= current_support + 1

    def increase_items_support(self, items):
        if items == None : return
        if len(items) == 0 : return

        for idx, item in enumerate(items):
            self.increase_support(frozenset([item]))

    def prune(self, df):
        start = df.shape[0]
        matches = df[df[self.SUPPORT] < self.min_support]
        df.drop(matches.index, inplace=True)
        logger.debug("Pruned patterns (k=%s) %s to %s", self.k, start, df.shape[0])
        return df[self.PATTERN]

    #def check_candidates(self, candidates):
    #    if candidates == None : return []
    #    if len(candidates) == 0 : return []

    #    patterns = set()
    #    for idx, candidate in enumerate(candidates):
    #        keep = True
    #        for idx2, item in enumerate(candidate):
    #            item = frozenset([item])
    #            keep = keep and self.is_frequent_pattern(item)
    #            if not keep: break

    #        if keep: patterns.add(candidate)

    #    return frozenset(patterns)

    def generate_pattern(self, item1, item2):
        pattern = item1.union(item2)
        if len(pattern) != self.k : return None
        return pattern

    def candidate_patterns(self, itemset, k):
        for idx, item in enumerate(itemset):
            for idx2, item2 in enumerate(itemset[idx+1:]):
                pattern = self.generate_pattern(item, item2)
                if pattern and not self.has_infrequent_subset(pattern):
                    yield pattern

    def apriori(self):
        """
        Ck = candidates
        """
        Lk = self.find_frequent_1_itemset()

        while len(Lk) > 0:

            Ck = self.apriori_gen(Lk)
            k_patterns = self.process_k(Ck)

            if len(k_patterns) == 0:
                logger.info( "No more frequents patterns" )
                break

            Lk = self.prune(k_patterns)

            self.frequents = pd.concat([self.frequents, k_patterns], ignore_index=True)

            self.k += 1

    def apriori_gen(self, Lk):
        #Lk = self.getLk_patterns()
        logger.info("Generating patterns for k[%s] = %s elements to match", self.k, len(Lk))
        candidates = set()
        combinations = itertools.combinations(Lk, 2)
        # TODO: review self join
        for comb in combinations:
            candidate = frozenset(comb[0].union(comb[1]))
            if len(candidate) == self.k:
                if not self.has_infrequent_subset(candidate):
                    candidates.add(candidate)

        #for candidate in self.candidate_patterns(Lk, self.k):
        #    candidates.append( candidate )

        logger.debug( "Candidates generated for k[%s] = %s", self.k, candidates)
        return list(candidates)

    def getLk_patterns(self):
        return list(self.frequents[self.frequents[self.K_TITLE] == (self.k - 1)][self.PATTERN])

    def process_k(self, candidates):
        #frequents = dict() #
        frequents = pd.DataFrame(columns=[self.K_TITLE, self.PATTERN, self.SUPPORT])
        logger.debug("Processing k=%s candidates: %s", self.k, candidates)
        for idx, row in self.categories.iterrows():
            itemset = row['categories']
            for candidate in candidates:
                if candidate.issubset(itemset): #.issuperset(candidate):
                    logger.debug("%s is subset of %s", candidate, itemset)
                    #frequents[candidate] = frequents.get(candidate, 0) + 1 #
                    self.increase_support(candidate, frequents)

        #dataset = pd.DataFrame(columns=[self.K_TITLE, self.PATTERN, self.SUPPORT])

        #for pattern, counter in frequents.items():
        #    dataset.loc[len(dataset)] = [self.k, pattern, counter]

        return frequents

    def has_infrequent_subset(self, pattern):
        #logger.debug("Checking subpatterns for %s", pattern)
        #items = list(pattern) # combinations can be created from set

        combinations = itertools.combinations(pattern, self.k - 1)

        for subpattern in combinations:
            subset = frozenset(subpattern)
            frequent = self.is_frequent_pattern(subset)

            if not frequent:
                logger.debug("Discarted %s because %s is not frequent", pattern, subpattern)
                return True

        #logger.debug("Pattern is a candidate: %s", valid)
        return False

    def is_frequent_pattern(self, item):
        return len(self.frequents[self.frequents[self.PATTERN] == item]) > 0
        return not self.frequents[self.frequents[self.PATTERN] == item].empty

    def write_to_file(self, filename='patterns.txt'):
        with open(filename, 'w') as f:
            for i, row in self.frequents.iterrows():
                f.write("%s:%s\n" % (row[self.SUPPORT], ";".join(row[self.PATTERN])))


if __name__ == '__main__':
    pass
#from pattern_discovery import PatternDiscovery
#import pandas as pd
#c = pd.read_pickle('data/categories.pickle.bz2')
#PD = PatternDiscovery(c)
#PD.apriori()

