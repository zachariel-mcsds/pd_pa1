#!/usr/bin/env python

import os
import pandas as pd
import logging
import itertools

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def testing_dataset():
    itemset = [
        [('b', 'd', 'c', 'b', 'a', 'c')],
        [('b','f','c','e','b','f','g')],
        [('a','h','b','f','a','b','f')],
        [('b','e','c','e','d')],
        [('a','b','d','b','c','b','a','d','e')],
    ]

    dataset = pd.DataFrame(itemset, columns=[SequentialPatternDiscovery.ITEMS])
    return dataset

def is_subsequence(candidate, itemset):
    present = False
    candidate_length = len(candidate)
    for idx, item in enumerate(itemset[:(-1 * candidate_length)]):
        patt = tuple(itemset[idx:idx+candidate_length])
        if patt == candidate:
            present = True
            break

    return present

def subsequences(candidate, k):
    subs = set()
    for idx, item in enumerate(candidate[:-k+1]):
        sub = candidate[idx:idx+k]
        subs.add(sub)
    return subs


def root_dir():
    """Project base directory"""
    return os.path.dirname(__file__)

def data_dir():
    """Data directory path"""
    return os.path.join(root_dir(), 'data')

def file_path():
    """Path to items file"""
    return os.path.join(data_dir(), 'reviews_sample.txt')

def data(filename):
    """Dataframe imported from file path"""

    df = pd.DataFrame(columns=[SequentialPatternDiscovery.ITEMS])
    for line in file_content(filename):
        df.loc[len(df)] = [line.strip().split(';')]

    return df

def file_content(filename):
    """Read file and yiel line by line"""

    with open(filename, 'r') as f:
        for line in f:
            yield line

def parse_items(str_value):
    """Parse string representation of set object"""

    str_value = str_value.replace('{','').replace('}', '')
    str_value = str_value.split(', ')
    str_value = set([s.replace('\'', '').strip() for s in str_value])
    return str_value

class SequentialPatternDiscovery:
    """ Class to implement Apriori algorithm over dataset"""

    PATTERN='pattern'
    SUPPORT='support'
    K_TITLE = 'k'
    ITEMS = 'items'

    def __init__(self, filename='data/reviews_sample.txt', min_support=100, csv=True):
        """Initizize object

        Params:
            filename: path to data file
            min_support: threshold to prune patterns
        """

        self.min_support = min_support
        self.k = 1
        self.items = pd.DataFrame(columns=[self.ITEMS])
        self.frequents = pd.DataFrame(columns=[self.K_TITLE, self.PATTERN, self.SUPPORT])

        if type(filename) is pd.DataFrame:
            self.items = filename
        else:
            self.filename = filename

            if not csv:
                self.import_file()

    def count(self, products):
        for product in products:
            self.counters.setdefault(product, 0)
            self.counters[product] += 1

    def read_file(self):
        logger.info("Reading file: %s",  self.filename)
        for line in file_content(self.filename):
            items = tuple(line.strip().split())
            yield items

    def import_file(self):
        for items in self.read_file():
            self.items.loc[len(self.items)] = [items]

    def find_frequent_1_itemset(self):
        logger.info("Generating single patterns")
        if self.items.empty:
            logger.debug("Reading from CSV file")
            for items in self.read_file():
                self.items.loc[len(self.items)] = [items]
                self.increase_items_support(items)
        else:
            logger.debug('Reading from DataFrame')
            for idx, row in self.items.iterrows():
                items = row[self.ITEMS]
                self.increase_items_support(items)
        self.prune(self.frequents)
        self.k = 2
        logger.debug("Total items: %s", self.items.shape[0])
        logger.debug("Total k=1 patterns: %s", self.frequents.shape[0])
        return self.frequents[self.frequents['k'] == 1][self.PATTERN]

    def increase_support(self, pattern, dF=None):
        logger.debug("Increasing support for %s", pattern);
        dataFrame = None
        if type(dF) is pd.DataFrame:
            dataFrame = dF
        else:
            dataFrame = self.frequents

        matches = dataFrame[(dataFrame[self.K_TITLE] == self.k) & (dataFrame[self.PATTERN] == pattern)]
        if matches.empty:
            #logger.debug("Adding pattern to frequents %s", pattern)
            dataFrame.loc[len(dataFrame)] = [self.k, pattern, 1]
        else:
            for idx in matches.index:
                dataFrame.at[idx, self.SUPPORT] += 1

    def increase_items_support(self, items):
        if items == None : return
        if len(items) == 0 : return

        counted = set()

        for idx, item in enumerate(items):
            pattern = tuple([item])
            if pattern not in counted:
                self.increase_support(pattern)
                counted.add(pattern)

    def prune(self, df):
        start = df.shape[0]
        matches = df[df[self.SUPPORT] < self.min_support]
        df.drop(matches.index, inplace=True)
        logger.debug("Pruned patterns (k=%s) %s to %s", self.k, start, df.shape[0])
        return df[self.PATTERN]

    def generate_pattern(self, item1, item2):
        pattern = item1.union(item2)
        if len(pattern) != self.k : return None
        return pattern

    def candidate_patterns(self, itemset, k):
        for idx, item in enumerate(itemset):
            for idx2, item2 in enumerate(itemset[idx+1:]):
                pattern = self.generate_pattern(item, item2)
                if pattern and not self.has_infrequent_subset(pattern):
                    yield pattern

    def GSP(self):
        """
        Ck = candidates
        """
        Lk = self.find_frequent_1_itemset()

        while len(Lk) > 0:

            Ck = self.apriori_gen(Lk)
            k_patterns = self.process_k(Ck)

            if len(k_patterns) == 0:
                logger.info( "No more frequents patterns" )
                break

            Lk = self.prune(k_patterns)

            self.frequents = pd.concat([self.frequents, k_patterns], ignore_index=True)

            self.k += 1

    def apriori_gen(self, Lk):
        #Lk = self.getLk_patterns()
        logger.info("Generating patterns for k[%s] = %s elements to match", self.k, len(Lk))

        if self.k == 2:
            candidates = set()
            for item1, item2 in itertools.permutations(Lk, 2):
                candidates.add(tuple(item1 + item2))

            for item in Lk:
                candidates.add(tuple(item + item))

            return  list(candidates)

        candidates = pd.DataFrame(Lk, columns=[self.PATTERN])
        for idx, row in candidates:
            suffix = row[self.PATTERN][1:]
            matches = candidates[candidates.apply(lambda x: x[self.PATTERN][:-1] == suffix)]
            print(matches)

        permutations = itertools.permutations(Lk, 2)
        # TODO: review self join
        prefix_length = k - 2
        for item1, item2 in permutations:
            permutations = itertools.permutations(item1+item2, k)

            for candidate in permutations:
                if len(candidate) == self.k:
                    if not self.has_infrequent_subsequences(candidate):
                        candidates.add(candidate)

        logger.debug( "Candidates generated for k[%s] = %s", self.k, candidates)
        return list(candidates)

    def getLk_patterns(self):
        return list(self.frequents[self.frequents[self.K_TITLE] == (self.k - 1)][self.PATTERN])

    def process_k(self, candidates):
        frequents = pd.DataFrame(columns=[self.K_TITLE, self.PATTERN, self.SUPPORT])
        logger.debug("Processing k=%s candidates: %s", self.k, candidates)
        for idx, row in self.items.iterrows():
            itemset = row[self.ITEMS]
            sequence = ' '.join(itemset)
            for candidate in candidates:
                subseq = ' '.join(candidate)
                #if is_subsequence(candidate, itemset):
                if subseq in sequence:
                    logger.debug("%s is subset of %s", candidate, itemset)
                    self.increase_support(candidate, frequents)

        return frequents

    def has_infrequent_subsequences(self, pattern):
        #logger.debug("Checking subpatterns for %s", pattern)
        #items = list(pattern) # combinations can be created from set

        combinations = itertools.combinations(pattern, self.k - 1)

        for subpattern in combinations:
            subset = frozenset(subpattern)
            frequent = self.is_frequent_pattern(subset)

            if not frequent:
                logger.debug("Discarted %s because %s is not frequent", pattern, subpattern)
                return True

        #logger.debug("Pattern is a candidate: %s", valid)
        return False

    def is_frequent_pattern(self, item):
        return len(self.frequents[self.frequents[self.PATTERN] == item]) > 0
        return not self.frequents[self.frequents[self.PATTERN] == item].empty

    def write_to_file(self, filename='patterns.txt'):
        with open(filename, 'w') as f:
            for i, row in self.frequents.iterrows():
                f.write("%s:%s\n" % (row[self.SUPPORT], ";".join(row[self.PATTERN])))


if __name__ == '__main__':
    pass
#from pattern_discovery import SequentialPatternDiscovery
#import pandas as pd
#c = pd.read_pickle('data/items.pickle.bz2')
#PD = SequentialPatternDiscovery(c)
#PD.apriori()

